;(function($) {

    $(function() {
        $("#selectRowValue, #tagFilter, #filterByIsFinished, #reviewStatus").change(function (e) {
            e.preventDefault();
            window.location = $(this).children('option:selected').data('url');
        });
    });

}(jQuery));
