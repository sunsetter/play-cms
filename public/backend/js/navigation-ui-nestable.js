var UINestable = function () {

    var updateOutput = function (e) {
        var list = e.length ? e : $(e.target),
            output = list.data('output');
        if (window.JSON) {
            output.val(window.JSON.stringify(list.nestable('serialize'))); //, null, 2));
        } else {
            output.val('JSON browser support required.');
        }

        // remove disabled class from submit button if serialized list has changed
        if ($('#navorder').data('initialData') != null
            && $('#navorder').data('initialData') != $('#navorder_output').val())
        {
            $('#btn_save').removeClass('disabled').addClass('btn-primary');
        }
    };

    var saveExpandedItemsInPreference = function (name, params) {
        var ids = [];

        $('#navorder li.dd-item > button[data-action="collapse"]:visible').each(function(i, elem) {
            ids.push($(elem).parent(".dd-item").data("id"))
        })

        $.post("/preference/save/NAV_ITEMS_EXPAND", { ids: ids });
    }

    var expandPreferenceItems = function () {
        $.get("/preference/get/nav_items_expand", function(data) {
            if (!data || !data.value) {
                return;
            }
            var ids = data.value.split(",");

            ids.forEach(function(v) {
                var li = $("li[data-id=" + v+"]");

                li.removeClass("dd-collapsed");
                li.children('[data-action="expand"]').hide();
                li.children('[data-action="collapse"]').show();
                li.children("ol").show();
            })
        });
    }

    return {
        //main function to initiate the module
        init: function () {

            // activate Nestable for list 1
            $('#navorder').nestable({
                group: 1
            })
                .on('change', updateOutput);

            $('#navorder').nestable('collapseAll');

//            // activate Nestable for list 2
//            $('#nestable_list_2').nestable({
//                group: 1
//            })
//                .on('change', updateOutput);

            // output initial serialised data
            updateOutput($('#navorder').data('output', $('#navorder_output')));

            // saves initial serialised data
            $('#navorder').data('initialData', $('#navorder_output').val());

//            updateOutput($('#nestable_list_2').data('output', $('#nestable_list_2_output')));

            $('#nestable_list_menu').on('click', function (e) {
                var target = $(e.target),
                    action = target.data('action');
                if (action === 'expand-all') {
                    $('.dd').nestable('expandAll');
                    saveExpandedItemsInPreference();
                }
                if (action === 'collapse-all') {
                    $('.dd').nestable('collapseAll');
                    saveExpandedItemsInPreference();
                }
            });

            $('#nestable_list_3').nestable();

            expandPreferenceItems();

            $(document).on("click", "#navorder .dd-item > button", function() {
                saveExpandedItemsInPreference();
            });
        }

    };

}();