/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.commons.util;

import akka.dispatch.OnFailure;
import play.db.jpa.JPA;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.libs.Akka;
import scala.PartialFunction;
import scala.concurrent.ExecutionContext;
import scala.concurrent.Future;

import java.util.function.Supplier;

import static akka.dispatch.Futures.future;

public class Async  {
	private final static Logger logger = LoggerFactory.getLogger(Async.class);

    @SuppressWarnings("unchecked")
    private static final PartialFunction<Throwable, ?> onFailure = (PartialFunction<Throwable, ?>) new OnFailure() {
        @Override
        public void onFailure(Throwable failure) throws Throwable {
            logger.error(failure.getMessage(), failure);
        }
    };

    public static Future<Void> call(final Runnable block) {
        final ExecutionContext ex = Akka.system().dispatcher();

        Future<Void> f = future(() -> {
            JPA.withTransaction(block);
            return null;
        }, ex);

        f.onFailure(onFailure, ex);

        return f;
    }

    public static <T> Future<T> call(final Supplier<T> block) {
        final ExecutionContext ex = Akka.system().dispatcher();

        Future<T> f = future(() -> {
            try {
                return JPA.withTransaction(block);
            } catch (Throwable t) {
                throw new RuntimeException(t);
            }
        }, ex);

        f.onFailure(onFailure, ex);

        return f;
    }
}
