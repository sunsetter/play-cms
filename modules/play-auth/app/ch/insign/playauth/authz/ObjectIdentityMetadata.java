/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.playauth.authz;

import com.google.common.collect.ImmutableMap;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public interface ObjectIdentityMetadata {

	<T> Optional<T> get(Class<T> type);


	public static class Builder {
		private Map<Class<?>, Object> metadata = new HashMap<>();

		public <T, V extends T> Builder add(Class<T> type, V object) {
			metadata.put(type, object);
			return this;
		}

		public ObjectIdentityMetadata build() {

			Map<Class<?>, Object> meta = ImmutableMap.copyOf(metadata);

			return new ObjectIdentityMetadata() {
				@Override
				public <T> Optional<T> get(Class<T> type) {
					return Optional.ofNullable(meta.get(type)).filter(type::isInstance).map(type::cast);
				}
			};
		}
	}
}
