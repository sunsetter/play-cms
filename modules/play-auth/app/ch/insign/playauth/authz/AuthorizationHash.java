/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.playauth.authz;

import org.apache.shiro.crypto.hash.Hash;

/**
 * A AuthorizationHash is a unique representation of the set of permissions.
 *
 *
 * If several parties have equal AuthorizationHash-es, then they're also equally treated by the Authorizer.
 *
 * The AuthorizationHash might be used as cache key to store authorization requests as for
 * the same AuthorizationHash the Authorizer will always return the same result.
 */
public interface AuthorizationHash extends Hash {
}
