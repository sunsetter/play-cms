/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.playauth.controllers.actions;

import ch.insign.playauth.PlayAuthApi;
import ch.insign.playauth.authz.DomainPermission;
import play.db.jpa.JPAApi;
import play.mvc.Http;
import play.mvc.Result;

import javax.inject.Inject;
import java.util.concurrent.CompletionStage;

/**
 * Ensures the calling subject has required {@link RequiresPermissionAction#permission(Http.Context ctx) permission}.
 *
 * Example usage:
 * {@code
 *   public class RequiresEditPartyPermissionAction extends RequiresPermissionAction<Void> {
 *       @Override
 *       public DomainPermission<?> permission(Http.Context ctx) {
 *           // define class-wide permission
 *           DomainPermission<Party> perm = PartyPermission.EDIT;
 *
 *           // retrieve a specific target to which permission applies
 *           String id = ctx.request().getQueryString("id");
 *           Party party = PlayAuth.getPartyManager().find(id);
 *
 *           // return party-specific permission or class-wide permission if party is not found
 *           return party != null ? PlayAuth.getPermissionManager().applyTarget(perm, party) : perm;
 *       }
 *   }
 * }
 */
abstract public class RequiresPermissionAction<T> extends WithSubjectAction<T> {

	@Inject
	JPAApi jpaApi;

	@Inject
	private PlayAuthApi playAuthApi;

	@Override
	public CompletionStage<Result> doCall(Http.Context ctx) {
		jpaApi.withTransaction(() -> playAuthApi.requirePermission(permission(ctx)));

		return this.delegate.call(ctx);
	}

	abstract public DomainPermission<?> permission(Http.Context ctx);
}
