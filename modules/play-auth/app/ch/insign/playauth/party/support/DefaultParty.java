/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.playauth.party.support;


import ch.insign.playauth.event.RoleGrantEvent;
import ch.insign.playauth.event.RoleRevokeEvent;
import ch.insign.playauth.party.Party;
import ch.insign.playauth.party.PartyIdentifier;
import ch.insign.playauth.party.PartyRole;
import ch.insign.playauth.party.address.EmailAddress;
import ch.insign.playauth.party.preference.Preference;
import ch.insign.playauth.realm.DefaultRealm;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.SimplePrincipalCollection;
import play.data.validation.Constraints;

import javax.persistence.*;
import java.util.*;

import static ch.insign.playauth.PlayAuth.getEventDispatcher;

@Entity
@Table(name = "auth_party")
@Inheritance (strategy=InheritanceType.SINGLE_TABLE)
public class DefaultParty implements Party  {

    @Id
    @GeneratedValue
    private long id;

    @Constraints.Required
    @Constraints.Email
    @Column(unique = true)
    private String email;

    private String name;

    private String credential;

    private boolean isLocked = false;

    @Transient
    private String password;

    @Transient
    private String repeatPassword;

    @ManyToMany
    @JoinTable(
            name = "auth_default_party_default_role",
            joinColumns = @JoinColumn(name = "USER_ID", referencedColumnName = "ID"),
            inverseJoinColumns = @JoinColumn(name = "ROLE_ID", referencedColumnName = "ID"))
    private Set<DefaultPartyRole> partyRoles = new HashSet<>();

    @ElementCollection
    @CollectionTable(name = "auth_party_preferences")
    @MapKey(name = "name")
    private Map<String, Preference> preferences = new HashMap<>();

    @Override
    public String getId() {
        return String.valueOf(id);
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Object getCredentials() {
        return this.credential;
    }

    public void setCredentials(String credentials) {
        this.credential = credentials;
    }

    public String getRepeatPassword() {
        return repeatPassword;
    }

    public void setRepeatPassword(String repeatPassword) {
        this.repeatPassword = repeatPassword;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public PrincipalCollection getPrincipals() {
		SimplePrincipalCollection principals = new SimplePrincipalCollection();

		// allow easier access through principals.byType(PartyIdentifier.class)
        if (getId() != null && !getId().equals("") &&  !getId().equals("0")) {
            principals.add(new PartyIdentifier(getId()), DefaultRealm.REALM_NAME);
        }

		// allow easier access through principals.byType(EmailAddress.class)
		principals.add(new EmailAddress(email), DefaultRealm.REALM_NAME);

		return principals;
	}

    @Override
    public Set<PartyRole> getRoles() {
        return new HashSet<>(partyRoles);
    }

    @Override
    public void addRole(PartyRole role) {
        partyRoles.add((DefaultPartyRole) role);
        ((DefaultPartyRole) role).getParties().add(this);
        getEventDispatcher().dispatch(new RoleGrantEvent(role, this));
    }

    @Override
    public void removeRole(PartyRole role) {
        partyRoles.remove((DefaultPartyRole) role);
        ((DefaultPartyRole) role).getParties().remove(this);
        getEventDispatcher().dispatch(new RoleRevokeEvent(role, this));
    }

    @Override
    public boolean hasRole(PartyRole role) {
        return partyRoles.contains((DefaultPartyRole) role);
    }

    public Set<Preference> getPreferences() {
        return new HashSet<>(preferences.values());
    }

    @Override
    public Optional<Preference> getPreference(String name) {
        return Optional.ofNullable(preferences.get(name));
    }

    @Override
    public void setPreference(Preference preference) {
        preferences.put(preference.getName(), preference);
    }

    @Override
    public Optional<String> getPreferredOption(String name) {
        return getPreference(name)
                .flatMap(Preference::getFirstOption);
    }

    @Override
    public void setPreferredOption(String name, String option) {
        preferences.computeIfAbsent(name, k -> new Preference(k, option));
        preferences.computeIfPresent(name, (k, oldPref) -> oldPref.prependOption(option));
    }

    @Override
    public Optional<Preference> removePreference(String name) {
        return Optional.ofNullable(preferences.remove(name));
    }

    @Override
    public boolean isLocked() {
        return isLocked;
    }

    @Override
    public void setLocked(boolean locked) {
        isLocked = locked;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", this.getId())
                .append("name", this.getName())
                .append("email", this.getEmail())
                .toString();
    }
}
