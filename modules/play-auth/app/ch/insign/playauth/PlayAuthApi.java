/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.playauth;

import ch.insign.playauth.authz.*;
import ch.insign.playauth.event.EventDispatcher;
import ch.insign.playauth.party.Party;
import ch.insign.playauth.party.PartyManager;
import ch.insign.playauth.party.PartyRoleManager;
import ch.insign.playauth.shiro.PlayShiroApi;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.credential.PasswordMatcher;
import org.apache.shiro.authc.credential.PasswordService;
import org.apache.shiro.authz.AuthorizationException;
import play.mvc.Http;

import java.util.Optional;
import java.util.function.Supplier;

/**
 * Provides access to authentication and authorization services.
 */
public interface PlayAuthApi {
    PlayShiroApi env();

    EventDispatcher getEventDispatcher();

    AccessControlList getAccessControlList();

    Authorizer getAuthorizer();

    AccessControlListVoter getAccessControlListVoter();

    AccessControlManager getAccessControlManager();

    PermissionManager getPermissionManager();

    PartyManager getPartyManager();

    PartyRoleManager getPartyRoleManager();

    ObjectIdentity getObjectIdentity(Object domainObject);

    SecurityIdentity getSecurityIdentity(Object authority);

    AuthorizationHash getAuthorizationHash(Object authority);

    Optional<String> getPartyIdentifier();

    Optional<Party> getCurrentParty();

    PasswordMatcher getPasswordMatcher();

    PasswordService getPasswordService();

    void authenticate(Party party, Http.Context ctx);

    void authenticate(Party party);

    /**
     * Binds a subject to the current thread.
     */
    void bind(Http.Context ctx);

    /**
     * Removes a subject from current thread.
     */
    void unbind();

    /**
     * Allows this Party to 'run as' or 'assume' another identity indefinitely.
     */
    void impersonate(Party party) throws NullPointerException, IllegalStateException;

    /**
     * Returns {@code true} if this {@code Party} is 'impersonating' another identity other than its original one or
     * {@code false} otherwise (normal {@code Party} state).  See the {@link #impersonate impersonate} method for more
     * information.
     */
    boolean isImpersonated();

    /**
     * Releases the current 'impersonate' (assumed) identity and reverts back to the previous 'pre impersonate'
     * identity that existed before {@code #impersonate impersonate} was called.
     */
    Optional<Party> endImpersonation();

    /**
     * Returns the previous 'pre impersonate' identity of this {@code Party} before assuming the current
     * {@link #impersonate impersonate} identity, or {@code null} if this {@code Party} is not operating under an assumed
     * identity (normal state).
     */
    Optional<Party> getPreviousParty();

    void login(AuthenticationToken token) throws AuthenticationException;

    void logout();

    boolean isAuthenticated();

    boolean isRemembered();

    boolean isAnonymous();

    /**
     * Returns {@code true} if anonymous party is granted the given permission
     */
    boolean isRestricted(DomainPermission<?> permission);

    /**
     * Returns {@code true} if anonymous party is granted the given permission on all objects of the given type.
     */
    <T, V extends T> boolean isRestricted(DomainPermission<T> permission, Class<V> target);

    /**
     * Returns {@code true} if anonymous party is granted the given permission on the given object.
     */
    <T, V extends T> boolean isRestricted(DomainPermission<T> permission, V target);

    /**
     * Returns {@code true} if current party is granted the given permission.
     */
    boolean isPermitted(DomainPermission<?> permission);

    /**
     * Returns {@code true} if current party is granted the given permission on all objects of the given type.
     */
    <T, V extends T> boolean isPermitted(DomainPermission<T> permission, Class<V> target);

    /**
     * Returns {@code true} if current party is granted the given permission on the given object.
     */
    <T, V extends T> boolean isPermitted(DomainPermission<T> permission, V target);

    /**
     * @throws AuthorizationException if current party is NOT granted the given permission.
     */
    void requirePermission(DomainPermission<?> permission) throws AuthorizationException;

    /**
     * @throws AuthorizationException if current party is NOT granted the given permission on all objects of the given type.
     */
    <T, V extends T> void requirePermission(DomainPermission<T> permission, Class<V> target) throws AuthorizationException;

    /**
     * @throws AuthorizationException if current party is NOT granted the given permission on the given object.
     */
    <T, V extends T> void requirePermission(DomainPermission<T> permission, V target) throws AuthorizationException;

    <T> T execute(Http.Context ctx, Supplier<T> block);

    void execute(Http.Context ctx, Runnable block);

    <T> T executeAs(Party party, Supplier<T> block);

    void executeAs(Party party, Runnable block);

    <T> T executeWithTransaction(Http.Context ctx, Supplier<T> block);

    void executeWithTransaction(Http.Context ctx, Runnable block);

    <T> T executeWithTransactionAs(Party party, Supplier<T> block);

    void executeWithTransactionAs(Party party, Runnable block);
}
