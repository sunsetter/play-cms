/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.models;

import ch.insign.commons.db.MString;
import ch.insign.commons.db.Model;
import play.data.validation.Constraints;
import play.db.jpa.JPA;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * An extension to the normal email templates sent in the admin backend when creating or editing a user
 *
 * IMPORTANT: If you have existing email templates in your application, you have to run the following command in
 * phpmyadmin after the CMS version update:
 * <pre>
 *     'UPDATE cms_email_template SET DTYPE = "EmailTemplate";'
 * </pre>
 */
@Entity
@Table(name = "cms_customer_email_template")
@DiscriminatorValue("CustomerEmailTemplate")
@NamedQueries(value = {
		@NamedQuery(
				name = "CustomerEmailTemplate.byUserFormType",
				query = "SELECT n FROM CustomerEmailTemplate n WHERE " +
						":types  MEMBER OF n.typeList")
})
public class CustomerEmailTemplate extends EmailTemplate {

	public static final CustomerEmailTemplateFinder find = new CustomerEmailTemplateFinder();

	@ElementCollection
	@Constraints.Required(message = "backend.customer.user.emailTemplate.typelist.mandatory.error")
	private List<UserFormType> typeList = new ArrayList<>();

	public static List<CustomerEmailTemplate> getEmailTemplatesEditFrom() {
		return CustomerEmailTemplate.find.byUserFormType(UserFormType.UPDATE).getResultList();
	}

	public static List<CustomerEmailTemplate> getEmailTemplatesCreateFrom() {
		return CustomerEmailTemplate.find.byUserFormType(UserFormType.CREATE).getResultList();
	}

	public List<UserFormType> getTypeList() {
		return typeList;
	}

	public void setTypeList(List<UserFormType> typeList) {
		this.typeList = typeList;
	}

	public enum UserFormType {
		CREATE,
		UPDATE
	}

	public static class CustomerEmailTemplateFinder extends Model.Finder<CustomerEmailTemplate> {
		private CustomerEmailTemplateFinder() {
			super(CustomerEmailTemplate.class);
		}

        private TypedQuery<CustomerEmailTemplate> byUserFormType(UserFormType type) {
            return JPA.em().createNamedQuery("CustomerEmailTemplate.byUserFormType", CustomerEmailTemplate.class).setParameter("types", type);
        }
	}

}