/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.attributeset.model;

import ch.insign.commons.db.Model;

import javax.persistence.*;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/**
 * This class represents a set of all the {@link Value}s associated to a specific entity for a
 * speicific {@link AttributeSet}.
 */
@Entity
@Table(name="cms_eav_attribute_set_instance")
public class AttributeSetInstance extends Model {

    @ManyToOne
    AttributeSet set;

    @Column(name = "ownerObject")
    private String ownerObject;

    // LinkedHashMap is used because the attributes in the AttributeSet are ordered in the database (@OrderColumn) and
    // we want to keep that order
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "instance", orphanRemoval = true)
    private Map<Attribute, Value> values = new LinkedHashMap<>();

    public Map<Attribute, Value> getValues() {
        return values;
    }

    public void setValues(Map<Attribute, Value> values) {
        this.values = values;
    }

    public Value get(Attribute attr){
        return values.get(attr);
    }

    public void put(Attribute attr, Value val){
        values.put(attr, val);
    }

    public void remove(Attribute attr) {
        values.remove(attr);
    }

    public Set<Map.Entry<Attribute, Value>> entrySet(){
        return values.entrySet();
    }

    public AttributeSet getSet() {
        return set;
    }

    public void setSet(AttributeSet set) {
        this.set = set;
    }

    public String getOwnerObject() {
        return ownerObject;
    }

    public void setOwnerObject(String ownerObject) {
        this.ownerObject = ownerObject;
    }
}
