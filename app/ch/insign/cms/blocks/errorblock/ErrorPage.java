/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.blocks.errorblock;

import ch.insign.cms.models.BlockCache;
import ch.insign.cms.models.BlockFinder;
import ch.insign.cms.models.PageBlock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.twirl.api.Html;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "cms_error_page")
@DiscriminatorValue("CmsErrorPage")
//@SecuredEntity(name = "ErrorPage", actions = {"read", "read_message", "modify"})
public class ErrorPage extends PageBlock  {
	private final static Logger logger = LoggerFactory.getLogger(ErrorPage.class);

    public final static String KEY_NOT_FOUND = "_notfound";
    public final static String KEY_UNAVAILABLE = "_unavailable";
    public final static String KEY_INTERNAL_ERROR = "_internalError";
    public final static String KEY_FORBIDDEN = "_forbidden";

    @Transient
    private String errorMsg;

    @Transient
    private String errorId;

    public static BlockFinder<ErrorPage> find = new BlockFinder<>(ErrorPage.class);

    @Override
    public Html render() {
        return ch.insign.cms.blocks.errorblock.html.errorShow.render(this);
    }

    @Override
    public boolean isSearchable() {
        return false;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getErrorId() {
        return errorId;
    }

    public void setErrorId(String errorId) {
        this.errorId = errorId;
    }

	/**
	 * No caching for the error page.
	 */
	@Override
	public BlockCache cache() {
		if (cache == null) {
			cache = new BlockCache(this);
			cache.setEnabled(false);
		}
		return cache;
	}
}
