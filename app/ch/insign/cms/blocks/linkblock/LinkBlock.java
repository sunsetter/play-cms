/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.blocks.linkblock;

import ch.insign.cms.models.AbstractBlock;
import ch.insign.cms.models.BlockFinder;
import ch.insign.cms.models.NavigationItem;
import ch.insign.cms.models.PageBlock;
import ch.insign.commons.db.MString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.twirl.api.Html;
import play.data.Form;
import play.mvc.Controller;

import javax.persistence.*;
import java.util.Optional;

/**
 *
 */
//@SecuredEntity(name = "LinkBlock", actions = {"read", "modify"})
@Entity
@Table(name = "cms_block_link")
@DiscriminatorValue("LinkBlock")
public class LinkBlock extends PageBlock  {
	private final static Logger logger = LoggerFactory.getLogger(LinkBlock.class);

    public static BlockFinder<LinkBlock> find = new BlockFinder<>(LinkBlock.class);

    @OneToOne(cascade= CascadeType.ALL)
    private MString linkTarget = new MString();


    public MString getLinkTarget() {
        return linkTarget;
    }

    public void setLinkTarget(MString linkTarget) {
        this.linkTarget = linkTarget;
    }

    @Override
    public void save() {
        for (NavigationItem navItem : this.getNavItems().values()) {
            navItem.setVisible(true);
        }
        super.save();
    }

    /**
     * Links are always visible.
     * @return
     */
    @Override
    public boolean isVisible() {
        return true;
    }

    /* TODO: Validation, as below. Problem: MString objects are still empty when validate() is called.. */
//    public List<ValidationError> validate() {
//        List<ValidationError> errors = new ArrayList<ValidationError>();
//
//        // At least one resolvable link needs to be added
//        if (getLinkTarget().get().isEmpty()) {
//            errors.add(new ValidationError("linkTarget." + Language.getDefaultLanguage(), "Please add a link."));
//            return errors;
//        }
//
//        // Check any given strings
//        for (String lang : Language.getAllLanguages()) {
//
//            if (!getLinkTarget().get(lang).isEmpty() &&
//                    !getLinkTarget().get(lang).startsWith("/") &&
//                    !getLinkTarget().get(lang).contains("://")
//                    ) {
//                errors.add(new ValidationError("linkTarget." + lang, "Invalid url. Please add a full url or a local one starting with /."));
//                return errors;
//            }
//        }
//
//        return null;
//    }


    /**
     * This handler lets you intercept url requests and define your own url (based on the selected nav item)
     *
     * @param navItem The item for which to return a url
     * @return a url (/some/local/path or http://www.fullexternal.com/some/path) or null to let the navitem decide
     */
    @Override
    public Optional<String> onGetUrl(NavigationItem navItem) {
        String url = getLinkTarget().get(navItem.getLanguage(), true);
        if (url.isEmpty()) {
            logger.warn("Link target empty, can't return working url for " + this);

	        // Note, if no url was found we will return "" and not empty, otherwise the calling code will
	        // generate a generic url of type /page/id - which will lead nowhere in case of a LinkBlock (which just links to another block)
	        return Optional.of("");
        }

        return Optional.of(url);
    }

    /**
     * Is the current page the one called in this request?
     * (Note: LinkBlock's links to urls with route entry differ from normal cms pages thus
     * we need to try and match by url path)
     */
    @Override
    public boolean isActive() {

        // if the selected nav item was set, use it as indicator
        if (super.isActive()) return true;

        String path = Controller.request().path();
        String link = getLinkTarget().get();
        boolean state = path != null && path.equals(link);
        return (state);
    }

    @Override
    public Html editForm(Form editForm) {
        return ch.insign.cms.blocks.linkblock.html.linkBlockEdit.render(this, (Form<LinkBlock>)editForm, Controller.request().getQueryString("backURL"), null);
    }

    @Override
    public Html render() {
        logger.error("A LinkBlock's render method should not be called (the call should be routed to the link target). Block: ", this);
        return Html.apply("");
    }


    @Override
    public AbstractBlock.BlockType getType() {
        return BlockType.PAGE;
    }


    @Override
    public boolean isSearchable() {
        return false;
    }
}
