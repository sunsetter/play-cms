/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.controllers;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import ch.insign.cms.models.AbstractBlock;
import play.mvc.Controller;

/**
 * Context (request scope) information about the current cms action being executed.
 * The action and block object are set in the controller.
 *
 * Note that the CmsContext can be null (e.g. early in the controller execution)
 *
 */
public class CmsContext  {
	private final static Logger logger = LoggerFactory.getLogger(CmsContext.class);

    public final static String CTXARG_KEY = "CmsContext";

    /**
     * Currently defined CMS actions
     */
    public static enum Action {
        SHOW, EDIT, SAVE, DELETE
    }

    private Action action;
    private AbstractBlock block;
	private String language;


    public CmsContext() {}

    public CmsContext(Action action, AbstractBlock block, String language) {
        this.action = action;
        this.block = block;
	    this.language = language;
    }

    /**
     * Get the current per-request CmsContext or null if none was set.
     * Controller.ctx() is used since it is per-request thread local
     */
    public static CmsContext current() {
        return (CmsContext) Controller.ctx().args.get(CTXARG_KEY);
    }

    /**
     * Set the current per-request CmsContext
     * Should be called from CMS controllers only.
     * @param ctx
     */
    public static void setCurrent(CmsContext ctx) {
        Controller.ctx().args.put(CTXARG_KEY, ctx);
    }

    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }

    public AbstractBlock getBlock() {
        return block;
    }

    public void setBlock(AbstractBlock block) {
        this.block = block;
    }

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}
}
