/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.inject.Inject;
import play.Logger;
import play.db.jpa.Transactional;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;
import play.routing.JavaScriptReverseRouter;


@With(GlobalActionWrapper.class)
@Transactional
public class ApplicationController extends Controller {

    @Inject
    public ApplicationController() {
    }

    public Result javascriptRoutes() {
	    long sec = 60 * 60 * 24 * 30;
	    response().setHeader("Cache-Control", "private, max-age=" + sec);
        return ok(JavaScriptReverseRouter.create("jsRoutes",
                ch.insign.cms.controllers.routes.javascript.BlockController.savePosition(),
                ch.insign.cms.blocks.reviewblock.controllers.routes.javascript.ReviewBackendController.list(),
                ch.insign.cms.blocks.reviewblock.controllers.routes.javascript.ReviewFrontendController.list(),
                ch.insign.cms.blocks.reviewblock.controllers.routes.javascript.ReviewFrontendController.addRatingToOldSubmission()
        )).as("text/javascript");
    }

    @BodyParser.Of(BodyParser.TolerantJson.class)
    public Result cspViolationReports() {
        JsonNode report = request().body().asJson().get("csp-report");
        Logger.of("CSP-Report").warn("CSP violation: {} violated on {}. Blocked (partial) uri: {}. Referrer: {}. IP: {}",
                report.get("violated-directive"),
                report.get("document-uri"),
                report.get("blocked-uri"),
                report.get("referrer"),
                request().remoteAddress()
        );

        return play.mvc.Results.ok();
    }
}
