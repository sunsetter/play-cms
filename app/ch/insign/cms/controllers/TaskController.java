/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.controllers;

import ch.insign.cms.blocks.backendlinkblock.BackendLinkBlock;
import ch.insign.cms.permissions.ApplicationPermission;
import ch.insign.cms.utils.Error;
import com.google.inject.Inject;
import ch.insign.commons.db.Paginate;
import ch.insign.commons.util.Task;
import ch.insign.commons.util.Task.TaskFinder;
import ch.insign.playauth.PlayAuth;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.db.jpa.JPA;
import play.db.jpa.JPAApi;
import play.db.jpa.Transactional;
import play.i18n.MessagesApi;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@With({GlobalActionWrapper.class, CspHeader.class})
@Transactional
public class TaskController extends Controller {
    public static final int ITEMS_PER_PAGE_DEFAULT = 50;
    private static final Logger logger = LoggerFactory.getLogger(TaskController.class);

    private MessagesApi messagesApi;
    private JPAApi jpaApi;

    @Inject
    public TaskController(MessagesApi messagesApi, JPAApi jpaApi) {
        this.messagesApi = messagesApi;
        this.jpaApi = jpaApi;
    }

    @SuppressWarnings("unchecked")
    public Result list(int page, int itemsPerPage, String sortBy, String order, String filterByTag, int filterByIsFinished, String reviewStatus) {
        itemsPerPage = itemsPerPage > 0 ? itemsPerPage : ITEMS_PER_PAGE_DEFAULT;

        CriteriaBuilder criteriaBuilder = JPA.em().getCriteriaBuilder();
        CriteriaQuery query = criteriaBuilder.createQuery();
        Root<Task> from = query.from(Task.class);

        // Apply filter
        List<Predicate> predicates = new ArrayList<>();

        if (StringUtils.isNotBlank(filterByTag)) {
            predicates.add(criteriaBuilder.equal(from.get("tag"), filterByTag));
        }

        if (filterByIsFinished == TaskFinder.FINISHED_TASK) {
            predicates.add(criteriaBuilder.equal(from.get("finished"), true));
        } else if (filterByIsFinished == TaskFinder.OPEN_TASK) {
            predicates.add(criteriaBuilder.equal(from.get("finished"), false));
        } else if (filterByIsFinished == TaskFinder.FAILED_TASK) {
            predicates.add(criteriaBuilder.equal(from.get("finished"), true));
            predicates.add(criteriaBuilder.equal(from.get("success"), false));
        }

        if (reviewStatus != null && !"".equals(reviewStatus)) {
            predicates.add(criteriaBuilder.equal(from.get("reviewStatus"), Task.ReviewStatus.valueOf(reviewStatus)));
        }

        query.where(criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()])));

        // Sorting
        if (StringUtils.isBlank(sortBy)) {
            query.orderBy(criteriaBuilder.desc(from.get("created")), criteriaBuilder.desc(from.get("finished")));
        } else if (order.equals("asc")) {
            query.orderBy(criteriaBuilder.desc(from.get(sortBy)));
        } else {
            query.orderBy(criteriaBuilder.asc(from.get(sortBy)));
        }

        // Get total count of results
        query.select(criteriaBuilder.count(from));
        Long count = (Long) JPA.em().createQuery(query).getSingleResult();

        // Get pagination for page list
        TypedQuery<Task> typedQuery = JPA.em().createQuery(query.select(from));
        Paginate<Task> pages = new Paginate<>(typedQuery, itemsPerPage, count.intValue());

	    return ok(ch.insign.cms.views.html.admin.tasksQueues.render(pages, page, sortBy, order, filterByTag, filterByIsFinished, reviewStatus));
    }

    public Result taskDetailView(String taskId) {
        Task task = Task.find.byId(taskId);

	    if (task == null) {
		    return Error.notFound("Task not found: " + taskId);
	    }

        // tell the cms webservice-log nav entry is active
        if (BackendLinkBlock.find.byKey("_backend_webservice_log") != null) {
            BackendLinkBlock.find.byKey("_backend_webservice_log").setActive(true);
        }

        return ok(ch.insign.cms.views.html.admin.taskDetailView.render(task));
    }

    public Result delete(String taskId, String backUrl) {
        Task task = Task.find.byId(taskId);

        if (task == null) {
            return Error.notFound("Task not found: " + taskId);
        }

        task.delete();

        return redirect(backUrl);
    }

    public Result executeTask(String taskId) {
        PlayAuth.requirePermission(ApplicationPermission.EXECUTE_MAINTENANCE_TASK);
        Task task = Task.find.byId(taskId);
        if (task == null) {
            return Error.notFound("Task not found: " + taskId);
        }

        if (task.getFirstTry() == null) {
            task.setFirstTry(Instant.now());
        }
        task.setLastTry(Instant.now());
        task.setFinished(false);
        task.setScheduleAfter(Instant.now());

        task.setTries(task.getTries() + 1);

        task.addLogEntry("Manually started task execution by " + PlayAuth.getCurrentParty().getEmail());
        if (task.execute()) {
            task.setSuccess(true);
            task.setFinished(true);
        } else {
            task.setSuccess(false);
            if (task.getTries() >= task.getMaxTries()) {
                task.setFinished(true);
            }
            task.failed();
        }

        task.save();
        return redirect(ch.insign.cms.controllers.routes.TaskController.taskDetailView(taskId));
    }

    public Result doChangeReviewStatus(String taskId, String reviewStatusString) {
        Task task = Task.find.byId(taskId);
        Task.ReviewStatus reviewStatus = null;

        try {
            reviewStatus = Task.ReviewStatus.valueOf(reviewStatusString);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }

        if (task == null || reviewStatus == null) {
            return Error.notFound("Task or review not found: " + taskId);
        }

        task.setReviewStatus(reviewStatus);
        task.save();

        return ok(ch.insign.cms.views.html.admin.reviewButtonPartial.render(task));
    }

    public Result saveNote(String taskId) {
        Task task = Task.find.byId(taskId);
        String note = null;

        if (request().body().asFormUrlEncoded().get("note") != null) {
            note = request().body().asFormUrlEncoded().get("note")[0];
        }

        if (task == null || note == null) {
            return Error.internal("bad request");
        }

        task.setNote(note.trim());
        task.save();

        flash("success", messagesApi.get(lang(), "task.flash.updated"));
        return redirect(ch.insign.cms.controllers.routes.TaskController.taskDetailView(taskId));
    }
}
