/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.controllers;

import ch.insign.cms.models.CMS;
import ch.insign.cms.views.js.js._config;
import ch.insign.cms.views.js.js._translations;
import ch.insign.playauth.PlayAuth;
import com.google.inject.Inject;
import play.db.jpa.JPAApi;
import play.db.jpa.Transactional;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;
import play.twirl.api.Content;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.HashSet;

import static ch.insign.cms.controllers.GlobalActionWrapper.KEY_PREF_BACKEND_LANG;
import static ch.insign.commons.util.Etag.resultWithETag;

@Transactional
@With({GlobalActionWrapper.class, CspHeader.class})
public class JsController extends Controller {

    private JPAApi jpaApi;

    @Inject
    public JsController(JPAApi jpaApi) {
        this.jpaApi = jpaApi;
    }

    /**
     * Receives a comma-separated string of language-keys which are parsed by the scala.js template to provide the client
     * with a js-translation-object containing all translated strings in the current language
     * @param commaSeparatedTranslationKeys comma-separated string of translation keys
     * @return javascript object called Messages containing the translated keys
     */
    public Result javascriptTranslations(String commaSeparatedTranslationKeys) {
        HashSet<String> keys = new HashSet<>(Arrays.asList(
                commaSeparatedTranslationKeys.split(",")));
        return ok(_translations.render(keys));
    }

    /**
     * Renders a scala.js template to provide initial javascript configuration objects used to initialize fronted libraries
     * Since the _config-template is parsed on page-load it is also a convenient place to add Translatable-Options for Javascript-libraries
     * Edit _config.js (Template) to add more init-objects or overwrite it as you wish in your web-app by using the app-specific version
     * of JSController (server.controllers.JSController)
     * This mechanism was introduced because of too many inline javascripts (for js-lib-configuration) we had in code previously.
     *
     * Since the config is used in frontend and backend with the same url path, this method needs to check
     * the referer to see if we are in the backend. If the referer is the backend, then it sets the current language
     * to the backend language. If there is no referer, or if the user has no preferences for the backend language,
     * the default mechanism kicks in.
     *
     * @return javascript object called "jsconfig" with global javascript-config variables
     */
    public Result config() {
        if (request().getHeader("referer") != null){
            try {
                URI referer = new URI(request().getHeader("referer"));
                if (referer.getPath().startsWith(CMS.getConfig().backendPath())){
                    jpaApi.withTransaction(() -> {
                        PlayAuth.getCurrentParty()
                                .getPreferredOption(KEY_PREF_BACKEND_LANG)
                                .ifPresent(Controller::changeLang);
                    });
                }
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        }
        return resultWithETag(parseJsConfig());
    }

    /**
     * renders the template and returns it as Content-Object
     * @return the Content-Object which was rendered
     */
    public Content parseJsConfig() {
        return _config.render();
    }


}



